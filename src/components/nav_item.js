import React from 'react';

export default class NavItem extends React.Component {
    render() {
        return (
            <li className={`nav-item ${this.props.active}`}>
                <a className="nav-link" href={this.props.to}>{this.props.text}</a>
            </li>
        );
    }
}