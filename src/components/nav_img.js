import React from 'react';
import { Link } from 'react-router-dom';
 
export default class NavImg extends React.Component {
    render() {
        return (
            <>
            <div className="navbar-brand d-none d-sm-block py-3">
                <Link to="/"><img src={this.props.logo} width="300" alt="Logo"></img></Link>
            </div>
            <div className="navbar-brand d-sm-none">
                <Link to="/"><img src={this.props.phoneLogo} width="80" alt="Phone Logo"></img></Link>
            </div>
            </>
        );
    }
}