import React from 'react';
import ChurchesTrustLogo from '../img/ChurchTrust_PTD_transparent.png';
import ChasLogo from '../img/chas.png';
import { Link } from 'react-router-dom';

import { FACEBOOK_ICON, INSTAGRAM_ICON, LINKEDIN_ICON } from './constants';

import '../styles/card.css';
import '../styles/links.css';

export default class Footer extends React.Component {
    render() {
        return (   
          <div className="bg-jet mt-5">        
            <div className="container">
              <div className="row py-3 text-center text-md-left">
                
                <div className="col-lg"></div>
                
                <div className="col col-md-4 col-lg-4 mt-3">
                    <h5 className="text-uppercase mb-4">Services</h5>
                    <p><Link to="/masonry">{this.props.services[0]}</Link></p>
                    <p><Link to="/limeworks">{this.props.services[1]}</Link></p>
                    <p><Link to="/walling">{this.props.services[2]}</Link></p>
                    <p><Link to="/heritage">{this.props.services[3]}</Link></p>
                </div>

                <hr className="w-100 clearfix d-md-none"></hr>

                <div className="col col-md-4 mt-3">
                    <h5 className="text-uppercase mb-4">Navigate</h5>
                    <p><Link to="/">Home</Link></p>
                    <p><Link to="/testimonials">Testimonials</Link></p>
                    <p><Link to="/contact">Contact</Link></p>
                    <div className="text-nowrap">
                        <a href='https://www.facebook.com/skconservation/?ref=page_internal'><img src={FACEBOOK_ICON} alt="Facebook"/></a>
                        <a href='https://www.instagram.com/skconservation/'><img className="ml-3" src={INSTAGRAM_ICON} alt="Instagram"/></a>
                        <a href='https://www.linkedin.com/in/shaun-killops-330b4762/'><img className="ml-3" src={LINKEDIN_ICON} alt="LinkedIn"/></a>
                    </div>                       
                </div>

                <hr className="w-100 clearfix d-md-none"></hr>

                <div className="col-6 mx-auto mx-md-none col-md-4 col-lg-3 mt-3">
                    <a href="https://www.nationalchurchestrust.org/"><img className="d-block mx-auto w-100" src={ChurchesTrustLogo} alt="National Churches Trust Logo"/></a>
                    {/* <h5 className="text-muted mt-3">Member of the Churches Trust</h5> */}
                    <a href="https://www.chas.co.uk/"><img className="d-block mx-auto mt-4 w-50" src={ChasLogo} alt="CHAS logo"/></a>
                </div>
                
                <div className="col-lg"></div>
              </div>

              <hr></hr>

              <div className="row justify-content-center">
                        <div className="col-auto">
                            <p className="text-muted">Copyright SK Conservation 2018-2020</p>
                        </div>
                    </div>
            </div>
          </div>
        );
    }
}