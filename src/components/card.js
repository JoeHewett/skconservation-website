import React from 'react';
import { Link } from 'react-router-dom';
import '../styles/card.css';

export default class Card extends React.Component {
    render() {
        return (
          // This link will go to the project details, but for now it goes to Contact
            <Link to="/contact">
            <div className="card border-0">
                <img className="card-img-top img-fluid" src={this.props.img} alt="Card"/>
                <div className="card-body">
                  <h5 className="card-title phoneText lead">{this.props.title}</h5>
                  <p className="text-muted">{this.props.text}</p>
                </div>
            </div>
            </Link>
        );
    }
}