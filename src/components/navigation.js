import React from 'react';
import { Navbar, Nav, NavDropdown } from 'react-bootstrap';
import NavImg from './nav_img';
import logo from '../img/logo4.png';
import phoneLogo from '../img/phonelogo.png';

import '../styles/navbar.css';

// Get the default Navbar from React-Boostrap and customise it. 
export default class Navigation extends React.Component {
  render() {
    return (
      <Navbar collapseOnSelect expand="lg" bg="jet" variant="dark" className="fixed-top">
        <div className="container-md">
          <NavImg logo={logo} phoneLogo={phoneLogo}></NavImg>
          <Navbar.Toggle id="toggler" aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="ml-auto">

              <Nav.Link href="/about">About</Nav.Link>

              <NavDropdown title="Services" id="collasible-nav-dropdown">
                <NavDropdown.Item href="/masonry">{this.props.services[0]}</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="/limeworks">{this.props.services[1]}</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="/walling">{this.props.services[2]}</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="/heritage">{this.props.services[3]}</NavDropdown.Item>
              </NavDropdown>

              <Nav.Link href="/testimonials">Testimonials</Nav.Link>
              <Nav.Link href="/contact">Contact</Nav.Link>


            </Nav>
          </Navbar.Collapse>
        </div>
      </Navbar>

    );
  }
}