
import castle from '../img/stoneMasonryFixing/castle.jpeg';
import extension from '../img/stoneMasonryFixing/extension.jpeg';
import canopy1 from '../img/stoneMasonryFixing/canopy1.png';
import doorway from '../img/stoneMasonryFixing/doorway.jpeg';
import fp7 from '../img/stoneMasonryFixing/fp2.png';
import stairs from '../img/stoneMasonryFixing/stairs1.png';

import house1 from '../img/limeWorks/house2.jpeg';
import house2 from '../img/limeWorks/house1.jpeg';
import limewall2 from '../img/limeWorks/limewall2.jpeg';
import limewall1 from '../img/limeWorks/limewall1.jpeg';
import limewall3 from '../img/limeWorks/lime4.jpg';
import limewindow from '../img/limeWorks/lime2.png';

import wall1 from '../img/stoneWalling/wall5.jpg';
import wall2 from '../img/stoneWalling/wall2.jpg';
import wall11 from '../img/stoneWalling/doorfloor.jpeg';
import pillar from '../img/stoneWalling/pillar.jpeg';
import wall4 from '../img/stoneWalling/wall4.jpeg';
//import boyswall from '../img/stoneWalling/boyswall.jpeg';
import wall12 from '../img/stoneWalling/wall12.jpg'; 

import church3 from '../img/heritageWorks/church3.jpeg';
import church2 from '../img/heritageWorks/church2.jpeg';
import hackwood from '../img/heritageWorks/hackwood1.jpeg';
import herit3 from '../img/heritageWorks/herit3.JPG';
import blackstatue from '../img/heritageWorks/blackstatue.png';
import statue from '../img/heritageWorks/statue.jpeg';
import roof from '../img/heritageWorks/roof.jpeg';

import instagram from '../img/instagram.png';
import facebook from  '../img/facebook.png';
import linkedin from  '../img/linkedin.png';

export const FACEBOOK_ICON = facebook
export const INSTAGRAM_ICON = instagram
export const LINKEDIN_ICON = linkedin


export const ABOUT_TEXT_1 = "Shaun Killops, Managing Director, founded the company in 2007.  Prior to this Shaun worked as a Banker Mason sole trader and worked on numerous stone conservation projects.";
export const ABOUT_TEXT_2 = "SK Conservation is a team of fully qualified conservators and stone masons, specialising in all areas of carving, fixing and building conservation.  We use traditional methods and techniques sympathetic to each individual project. We have extensive experience working on listed buildings, private residences and public sector buildings such as churches and schools.";
export const ABOUT_TEXT_3 = "SK Conservation has experienced Banker and Fixing masons, who can undertake bespoke carving projects in all types of masonry.  We can undertake traditional lime pointing and rendering, offering a range of different mixes for variations in texture and colour.  We have experience in all types of stonework, including creating dry-stone walling and rubble walling in a traditional and rustic style.";
export const ABOUT_TEXT_4 = "We can repair and conserve existing stonework and masonry, using traditional methods best suited to the period and style of your property.";
export const ABOUT_TEXTS = [ABOUT_TEXT_1, ABOUT_TEXT_2, ABOUT_TEXT_3, ABOUT_TEXT_4]
export const ABOUT_IMG = blackstatue; 
export const ABOUT_TITLE = "About us";

export const SERVICE1_TEXT = "SK Conservation has experienced Banker and Fixing masons, who can undertake bespoke carving projects in all types of masonry.";
export const SERVICE2_TEXT = "SK Conservation can undertake traditional lime pointing and rendering, offering a range of different mixes for variations in texture and colour. A no-obligation sample patch can be provided free of charge before any works are agreed, so that we can find the best fit for your property.";
export const SERVICE3_TEXT = "SK Conservation has experience in all types of stonework, including creating dry-stone walling and rubble walling in a traditional and rustic style.";
export const SERVICE4_TEXT = "SK Conservation can repair and conserve existing stonework and masonry, using traidtional methods best suited to the period and style of your property.";

export const TESTIMONIAL1_TEXT = "'We would be happy to recommend Shaun and his team to anyone looking for experienced, well organised contractor, specialising in period buildings and materials. So far we were very impressed with his work and pleased having him on site. I’m sure we will continue working with Shaun on our project, providing his availability allows for it.' - Kevin, Dorset"; 
export const TESTIMONIAL2_TEXT = "'There were number of factors why we considered working with Shaun: 1) Very competent: we were very impressed with quality and finish of Shaun's previous work: all seemed to perform well after longer period of time and looked very neat. Talking to Shaun instantly realised that he's has wealth of knowledge in building conservation and understands important details of working with period buildings. Most contractors in building trade, sadly, don't. 2) Fair and clear pricing structure: Depending on the type of work we choose to work with Shaun on day rate basis or per meter rate. This gives us flexibility of choice and allow to budget better for certain work. Especially when it comes to repairs and all unforeseeable faults in building fabric etc. 3) Keen interest in our project and pride in their own work.' - Adrian, Bath"; 
export const TESTIMONIAL3_TEXT = "'The main reason we decided to start our cooperation with Shaun and his team was the quality of work. We were very impressed with all of his previous projects. We quickly realised that this was down to very methodical and careful approach and preparation of the work. This resulted in repairs which aren’t noticeable and very neat, originally looking stone work. This paired with experience in working with lime mortars and traditional building methods provided us with solidly pointed and repaired wall. We have worked with few stone masons previously. Their brief was identical to Shaun’s. In our opinion quality of work done by SK Conservation definitely stands out from other’s. Would happily rate this at 10 out of 10.' - Simon, Bristol"; 
export const TESTIMONIAL4_TEXT = "'Thanks to Shaun for an amazing job on our listed property in Somerset.  Shaun and his team’s knowledge is obvious from the minute you meet them and the quality of their work is second to none. On our project, they also put right many wrongs carried out by people far less skilled and knowledgeable in the past. The team were a delight to have on site, leaving it immaculate at the end of each working day and, in my experience, are head and shoulders above other firms operating in this space. If you have a renovation project and want the highest quality of workmanship and finish, I have no hesitation in recommending Shaun’s company. They were a pleasure to deal with from start to finish.'"; 
export const TESTIMONIALS = [TESTIMONIAL1_TEXT, TESTIMONIAL2_TEXT, TESTIMONIAL3_TEXT, TESTIMONIAL4_TEXT]

export const TESTIMONIAL1_TITLE = "Masonry in Dorset";
export const TESTIMONIAL2_TITLE = "Limeworks in Bath";
export const TESTIMONIAL3_TITLE = "Wall Rebuild in Bristol";
export const TESTIMONIAL4_TITLE = "Renovation in Somerset";
export const TESTIMONIAL_TITLES = [TESTIMONIAL1_TITLE, TESTIMONIAL2_TITLE, TESTIMONIAL3_TITLE, TESTIMONIAL4_TITLE];

export const TESTIMONIAL1_IMAGE = castle;
export const TESTIMONIAL2_IMAGE = extension;
export const TESTIMONIAL3_IMAGE = hackwood;
export const TESTIMONIAL4_IMAGE = house1;
export const TESTIMONIAL_IMGS = [TESTIMONIAL1_IMAGE, TESTIMONIAL2_IMAGE, TESTIMONIAL3_IMAGE, TESTIMONIAL4_IMAGE]

export const IMAGES1 =
[{
    src: castle,
    thumbnail: castle,
},{        
    src: extension,
    thumbnail: extension   
},{
    src: fp7,
    thumbnail: fp7,
},{
    src: canopy1,
    thumbnail: canopy1,    
},{
    src: doorway,
    thumbnail: doorway,  
},{
    src: stairs,
    thumbnail: stairs,
}]

export const IMAGES2 =
[{
    src: house1,
    thumbnail: house1
},{        
    src: house2,
    thumbnail: house2
},{
    src: limewall1,
    thumbnail: limewall1 
},{
    src: limewall2,
    thumbnail: limewall2  
},{
    src: limewall3,
    thumbnail: limewall3
},{
    src: limewindow,
    thumbnail: limewindow

}]

export const IMAGES3 =
[{
    src: wall1,
    thumbnail: wall1,
},{        
    src: wall2,
    thumbnail: wall2,
},{
    src: wall4,
    thumbnail: wall4,
},{
    src: wall11,
    thumbnail: wall11
},{
    src: pillar,
    thumbnail: pillar
},{
    src: wall12,
    thumbnail: wall12
}]

export const IMAGES4 =
[{
    src: church2,
    thumbnail: church2,
},{        
    src: church3,
    thumbnail: church3,
},{
    src: statue,
    thumbnail: statue
},{
    src: blackstatue,
    thumbnail: blackstatue,
},{
    src: hackwood,
    thumbnail: hackwood
},{
    src: herit3,
    thumbnail: herit3
},{
    src: roof,
    thumbnail: roof
}]