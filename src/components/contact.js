import React, { Component } from 'react'
import * as emailjs from 'emailjs-com'

import { Button, FormFeedback, Form, FormGroup, Container, Row, Col } from 'react-bootstrap'
class ContactForm extends Component {
    state = {
        name: '',
        email: '',
        subject: '',
        message: '',
    }

handleSubmit(e) {
    e.preventDefault()
    const { name, email, subject, message } = this.state
    let templateParams = {
        from_name: email,
        to_name: 'SK Conservation',
        subject: subject,
        message_html: message,
    }
    
    emailjs.send(
        'gmail',
        'template_FenlBypH',
        templateParams,
        'user_ACADYfSNvZTJRKhd5nVCm'
    )
    this.resetForm()
 }
resetForm() {
    this.setState({
        name: '',
        email: '',
        subject: '',
        message: '',
    })
}
handleChange = (param, e) => {
    this.setState({ [param]: e.target.value })
}

render() {
    return (
            <Container className="">
                <Row className="pt-5">
                    <Col className="col-11 col-lg-6 pt-5">
                        <p className="text-muted">Send us a message using the contact form below and we'll get back to you as soon as possible.</p>
                        <p className="text-muted">Alternatively, you can reach Shaun via email at <b>shaun@skconservation.co.uk</b></p>
                        <p className="text-muted">Or call Shaun on <b>07891 804582</b></p>
                        <Form className="contact-form pt-5" onSubmit={this.handleSubmit.bind(this)}>
                            <FormGroup controlId="formBasicEmail">
                                <label className="text-muted">Email Address</label>
                                <input
                                    type="email"
                                    name="email"
                                    value={this.state.email}
                                    className="form-control"
                                    onChange={this.handleChange.bind(this, 'email')}
                                    placeholder=""
                                />
                            </FormGroup>
                            <FormGroup controlId="formBasicName">
                                <label className="text-muted">Name</label>
                                <input
                                    type="text"
                                    name="name"
                                    value={this.state.name}
                                    className="form-control"
                                    onChange={this.handleChange.bind(this, 'name')}
                                    placeholder=""
                                />
                            </FormGroup>
                            <FormGroup controlId="formBasicSubject">
                                <label className="text-muted">Subject</label>
                                <input
                                    type="text"
                                    name="subject"
                                    className="form-control"
                                    value={this.state.subject}
                                    onChange={this.handleChange.bind(this, 'subject')}
                                    placeholder=""
                                />
                            </FormGroup>
                            <FormGroup controlId="formBasicMessage">
                                <label className="text-muted">Message</label>
                                <textarea 
                                    className="form-control" 
                                    onChange={this.handleChange.bind(this, 'message')} 
                                    type="textarea" 
                                    name="message" 
                                    id="message" 
                                    placeholder=""
                                    maxLength="6000" 
                                    rows="7"
                                    value={this.state.message}>
                                </textarea>
                            </FormGroup>
                            <Button variant="outline-dark" type="submit">
                            Submit
                            </Button>
                        </Form>
                    </Col>
                </Row>
      </Container>
    )
  }
}
export default ContactForm