import React from 'react';
import { Container, Row, Col } from 'react-bootstrap'; 
import Card from './card';

import Tile3 from '../img/productTiles/wall_small.jpeg';
import Tile4 from '../img/productTiles/heritage_small.JPG';
import Tile2 from '../img/productTiles/lime2_small.jpeg';
import Tile1 from '../img/productTiles/masonry_small.jpg';

import { Link } from 'react-router-dom';

export default class ServiceCards extends React.Component {
  render() {
    return (
      <Container>
        <Row className="mt-5">
          <Col className="col-6 col-md-3">                  
              <Link to="masonry"><Card to="service1" img={Tile1} title={this.props.services[0]}></Card></Link>                           
          </Col>
          <Col className="col-6 col-md-3">            
              <Link to="limeworks"><Card to="service2" img={Tile2} title={this.props.services[1]}></Card></Link>
          </Col>
          <Col className="col-6 col-md-3"> 
              <Link to="walling"><Card img={Tile3} title={this.props.services[2]}></Card></Link>
          </Col>
          <Col className="col-6 col-md-3"> 
              <Link to="heritage"><Card to="service4" img={Tile4} title={this.props.services[3]}></Card></Link>
          </Col>     
        </Row>
      </Container>
    );
  }
}