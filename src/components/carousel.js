import React from 'react'; 
import { Container, Carousel } from 'react-bootstrap';
import Slide1 from '../img/slides/hackwood2_compressed.jpg';
import Slide2 from '../img/slides/statue_compressed.jpg';
import Slide3 from '../img/slides/wall_compressed.jpg';
import Slide4 from '../img/slides/castle_compressed.jpg'

export default class Carousel1 extends React.Component {
    render() {
        return (
          <Container fluid className="px-0">
            <Carousel data-interval="2000">
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={Slide1}
                alt="First slide"
              />
              {/* <Carousel.Caption>
                <h3>First slide label</h3>
                <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
              </Carousel.Caption> */}
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={Slide2}
                alt="Second slide"
              />
          
              {/* <Carousel.Caption>
                <h3>Second slide label</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
              </Carousel.Caption> */}
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={Slide3}
                alt="Third slide"
              />
          
              {/* <Carousel.Caption>
                <h3>Third slide label</h3>
                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
              </Carousel.Caption> */}
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={Slide4}
                alt="Fourth slide"
              />
              {/* <Carousel.Caption>
                <h3>First slide label</h3>
                <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
              </Carousel.Caption> */}
            </Carousel.Item>
          </Carousel>
        </Container>
        );
    }
}