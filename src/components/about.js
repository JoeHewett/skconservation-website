import React from 'react';
import { Button, Row, Container, Col } from 'react-bootstrap'; 
import { Link } from 'react-router-dom';

export default class About extends React.Component {
  render() {
    return (
      <Container className="text-muted text-left mt-5">
        <Row className="">
          <Col className="">        
            <h3 className="mb-3">Specialists in Masonry and Stone Conservation</h3>          
            <p>SK Conservation is a team of fully qualified conservators and stone masons, specialising in all areas of carving, fixing and building conservation.</p>
            <p>We use traditional methods and techniques sympathetic to each individual project, and have extensive experience working on private residences and public sector buildings such as churches and schools.</p>
            {/* <p>We are able to work on either a main or sub contractor basis, and we offer a free consultation service prior to undertaking any projects, large or small.</p> */}
            {/* <p>We are based near Bath, and work mainly in the surrounding areas, including Bristol, Trowbridge, Chippenham and Gloucester, but can cover the whole of the U.K. when required. </p> */}                                            
            <Link to="/contact"><Button variant="outline-dark">Get A Free Quotation</Button></Link>
          </Col>
        </Row>
      </Container>
    );
  }
}