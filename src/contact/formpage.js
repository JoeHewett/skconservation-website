import React from 'react';
import ContactImg from '../img/contactus.png';

export default class Contact extends React.Component {
    render() {
        return (
            <>
            <div class="container py-4">
                <div class="row pt-5">
                    <div class="col-md-6 col-md-offset-3 col-centered centered">
                        <img src={ContactImg} class="py-4"></img>
                        <p>Send us a message using the contact form below and we'll get back to you as soon as possible.</p>
                        <p>Alternatively, you can email us at shaun@skconservation.co.uk</p>
                        <p>Or call us on 07891 804582</p>
                        
                        <form role="form" method="post" id="reused_form" >
                            <div class="form-group">
                                <label for="name"> Name:</label>
                                <input type="text" class="form-control" id="name" name="name" required maxlength="50"></input>
                            </div>
                            <div class="form-group">
                                <label for="email"> Email:</label>
                                <input type="email" class="form-control" id="email" name="email" required maxlength="50"></input>
                            </div>
                            <div class="form-group">
                                <label for="name"> Message:</label>
                                <textarea class="form-control" type="textarea" name="message" id="message" placeholder="Your Message Here" maxlength="6000" rows="7"></textarea>
                            </div>
                            <button type="submit" class="btn btn-lg btn-primary pull-right" id="btnContactUs">Send &rarr;</button>
                        </form>
                        <div id="success_message" style={{display:"none"}}> <h3>Sent your message successfully!</h3> </div>
                        <div id="error_message" style={{display:"none"}}> <h3>Error</h3> Sorry there was an error sending your form. Please try again or contact us using the email or phone number above. </div>
                    </div>
                </div>
            </div>
   
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        
        <script type="text/javascript" src="https://platform.linkedin.com/badges/js/profile.js" async defer></script>  
    
        </>

        );

    }
}
      

      