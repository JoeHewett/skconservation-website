import React from 'react';
import Gallery from 'react-grid-gallery';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default class Service1 extends React.Component {
  render() {
    return (
      <Container className="">
        <Row className="mt-5">
          <Col className="col-lg-6">
            <h3>{this.props.title}</h3>
            <p className="text-muted">{this.props.text} <br /><br />Contact us for a no-obligation consultation.</p>

            <Link to="/contact"><Button variant="outline-dark">Get In Touch</Button></Link>
          </Col>
        </Row>
        <Row className="pt-5">
          <Col className="col-12">
            <Gallery rowHeight={200} images={this.props.images} enableImageSelection={false} backdropClosesModal={true} />
          </Col>
        </Row>

      </Container>
    );
  }
}

