import React from 'react';
import Carousel from './components/carousel.js';
import ServiceCards from './components/service_cards.js';
import About from './components/about.js';

import './App.css';

function Home() {

  const services = ['Stone Masonry & Fixing', 'Traditional Lime Works', 'Stone Work & Walling', 'Heritage & Church Works'];

  return (
    <div className="App">
    
        <Carousel />

        <About />
        
        <ServiceCards services={services}/>
        
    </div>
  );
}

export default Home;
