import React from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import './App.css';

export default class Testimonials extends React.Component {
  render() {
    return (
      <Container className="mt-5">
        <Row className="justify-content-center">
          
          <Col className="col-7 col-lg-4">
            <img src={this.props.img} className="d-block mx-auto w-100" alt="Picture of a black statue on a red stone base."/>               
          </Col>    

          <Col className="col-12 col-lg-8 mt-4 mt-lg-0">
            <h3>{this.props.title}</h3>
            <p className="text-muted">{this.props.text[0]}</p>
            <p className="text-muted">{this.props.text[1]}</p>
            <p className="text-muted">{this.props.text[2]}</p>
            <p className="text-muted">{this.props.text[3]}</p>
            <Link to="/contact"><Button variant="outline-dark">Get In Touch</Button></Link>
          </Col>
                
        </Row>
      </Container>
    );
  }
}