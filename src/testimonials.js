import React from 'react';
import './App.css';
import { Container, Row, Col } from 'react-bootstrap';
import Card from './components/card.js';

export default class Testimonials extends React.Component {
  render() {
    return (
      <Container className="pt-5">
        <Row className="pt-5">

          <Col className="col-12 col-md-6">
            <Card img={this.props.imgs[2]} title={this.props.title[2]} text={this.props.text[2]}></Card>
            <Card img={this.props.imgs[0]} title={this.props.title[0]} text={this.props.text[0]}></Card>
          </Col>

          <Col className="col-12 col-md-6">
            <Card img={this.props.imgs[3]} title={this.props.title[3]} text={this.props.text[3]}></Card>
            <Card img={this.props.imgs[1]} title={this.props.title[1]} text={this.props.text[1]}></Card>
          </Col>

        </Row>
      </Container>
    );
  }
}