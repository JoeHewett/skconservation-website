import React from 'react';

import Testimonials from './testimonials.js';
import Home from './home.js';
import Service from './service.js';
import Contact from './components/contact.js';
import AboutPage from './about_page.js';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import MyNavbar from './components/navigation.js'
import Footer from './components/footer.js';
import * as Constants from './components/constants.js';

import './App.css';
import './styles/headers.css';

function App() {

  const services = ['Stone Masonry & Fixing', 'Traditional Lime Works', 'Stone Work & Walling', 'Heritage & Church Works'];

  return (
    <Router>

      <MyNavbar services={services} />

      <Switch>
        <Route exact strict path="/">
          <Home />
        </Route>

        <Route exact path="/contact">
          <Contact />
        </Route>

        <Route exact path="/about">
          <AboutPage title={Constants.ABOUT_TITLE} img={Constants.ABOUT_IMG} text={Constants.ABOUT_TEXTS} />
        </Route>

        <Route exact path="/testimonials">
          <Testimonials title={Constants.TESTIMONIAL_TITLES} text={Constants.TESTIMONIALS} imgs={Constants.TESTIMONIAL_IMGS} />
        </Route>

        <Route exact path="/masonry">
          <Service title={services[0]} text={Constants.SERVICE1_TEXT} images={Constants.IMAGES1} />
        </Route>

        <Route exact path="/limeworks">
          <Service title={services[1]} text={Constants.SERVICE2_TEXT} images={Constants.IMAGES2} />
        </Route>

        <Route exact path="/walling">
          <Service title={services[2]} text={Constants.SERVICE3_TEXT} images={Constants.IMAGES3} />
        </Route>

        <Route exact path="/heritage">
          <Service title={services[3]} text={Constants.SERVICE4_TEXT} images={Constants.IMAGES4} />
        </Route>
      </Switch>

      <Footer services={services} />

    </Router>
  );
}

export default App;
